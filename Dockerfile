FROM python:3.7-alpine3.12
# TODO Set metadata for who maintains this image
COPY * /app/
RUN apk add curl
RUN pip3 install Flask --user
EXPOSE 8080
#TODO Set default values for env variables
ENV ENVIRONMENT="DEV"
ENV DISPLAY_FONT="arial"
ENV DISPLAY_COLOR="red"
#TODO *bonus* add a health check that tells docker the app is running properly
HEALTHCHECK CMD curl --fail http://localhost:8080/ || exit 1
# TODO have the app run as a non-root user
USER 1000
CMD python3 /app/app.py
